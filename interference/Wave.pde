class Wave {
  float amp = 10, phase, k = TAU / 32, w = 0.05, beta = 0.01;
  int x0, y0;

  Wave(int x, int y) {
    x0 = x;
    y0 = y;
  }

  float getAmp(int x, int y, int t) {
    float r = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
    return amp * sin(r * k - w * t + phase);
    // Затухающие
    // return amp * exp(-beta * r) * cos(r * k - w * t + phase);
  };
};
