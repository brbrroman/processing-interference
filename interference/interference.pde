/*
 * Language: Processing 3.3.0
 * Author:   Vekshin Roman
 * Date:     03.01.2017
 */
 
Wave[] waves;
byte size = 2, maxAmp = 20;
int colBase = #000000;

void setup() {
  size(768, 768);
  waves = new Wave[size];
  for (byte k = 0; k < size; ++k)
    waves[k] = new Wave(width / 2 + floor(200 * cos(TAU * k / size)),
                        height / 2 + floor(200 * sin(TAU * k / size)));
  loadPixels();
}

int t = 0;
void draw() {
  for (short i = 0; i < width; ++i)
    for (short j = 0; j < height; ++j) {
      float inPoint = 0;
      for (byte k = 0; k < size; ++k)
        inPoint += waves[k].getAmp(i, j, frameCount);
      if (inPoint > 0)
        pixels[i + j * width] = colBase + 65536 * floor(256 * inPoint / maxAmp);
      else
        pixels[i + j * width] = colBase + floor(-256 * inPoint / maxAmp);
    }
  updatePixels();
}
